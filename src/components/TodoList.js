import React, { Component } from "react";
import TodoItem from "./TodoItem.js"

class TodoList extends Component {
    render() {
        return (
            <section className="main">
                <ul className="todo-list">
                    {this.props.todos.map(todo => (
                        <TodoItem title={todo.title}
                            completed={todo.completed}
                            handleToggle={this.props.handleToggle(todo)}
                            handleClickDestroy={this.props.handleClickDestroy(todo)} />
                    ))}
                </ul>
            </section>
        );
    }
}

export default TodoList

