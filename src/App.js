import React, { Component } from "react";
import "./index.css";
import todosList from "./todos.json";
import TodoList from "./components/TodoList"
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";


class App extends Component {
  state = {
    todos: todosList,
    input: ''
  };
  handleToggle = todo => event => {
    let tempState = this.state
    todo.completed = !todo.completed
    this.setState({ tempState })
  }
  handleClearComplete = event => {
    let tempState = this.state
    let tempTodos = this.state.todos.filter(
      todo => todo.completed === false
      )
      tempState.todos = tempTodos
      this.setState({ tempState })
    }

    handleClickDestroy = todo => event => {
      let tempState = this.state
      let newTodos = tempState.todos.filter(
      item => item.id !== todo.id)
      this.setState({ todos: newTodos })
    }

  handleCreateTodo = event => {
    if (event.keyCode === 13) {
      let tempState = this.state;

      tempState.todos.push(
        {
          userId: 1,
          id: Math.ceil(Math.random() * 100000),
          title: tempState.input,
          completed: false
        }
      )
      this.setState({ todos: tempState.todos, input: '' });
      event.target.value = ''
    }
  }
  handleChange = event => {
    this.setState({ input: event.target.value })
  }
  render() {
    return (
      <Router>
        <section className="todoapp">
          <header className="header">
            <h1>todos</h1>
            <input
              className="new-todo"
              placeholder="What needs to be done?"
              autoFocus
              value={this.state.input}
              onChange={this.handleChange}
              onKeyDown={this.handleCreateTodo}
            />
          </header>
          <Route exact path='/' render={() => (
            <TodoList todos={this.state.todos}
              handleToggle={this.handleToggle}
              handleClickDestroy={this.handleClickDestroy} />
          )} />
          <Route path='/active' render={() => (
            <TodoList todos={this.state.todos.filter(todo => {
              if(todo.completed === false ){
                return todo
              }
              return false
            })}
              handleToggle={this.handleToggle}
              handleClickDestroy={this.handleClickDestroy} />
          )} />
          <Route path='/completed' render={() => (
            <TodoList todos={this.state.todos.filter(todo => {
              if(todo.completed === true){
                return todo
              }
              return false
            }
            )}
              handleToggle={this.handleToggle}
              handleClickDestroy={this.handleClickDestroy} />
          )} />
          <footer className="footer">
            {/* <!-- This should be `0 items left` by default --> */}
            <span className="todo-count">
              <strong>{this.state.todos.filter(todo => {
              if(todo.completed === false ){
                return todo
              }
              return false
            }).length}</strong> item(s) left
          </span>
            <ul className="filters">
              <li>
                <NavLink exact to='/' activeClassName = 'selected'>All</NavLink>
              </li>
              <li>
                <NavLink to="/active" activeClassName = 'selected'>Active</NavLink>
              </li>
              <li>
                <NavLink to="/completed" activeClassName = 'selected'>Completed</NavLink>
              </li>
            </ul>
            <button className="clear-completed" onClick={this.handleClearComplete}>Clear completed</button>
          </footer>
        </section>
      </Router>
    );
  }
}

export default App;


